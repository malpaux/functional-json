/**
 * @file Master entrypoint
 */

/**
 * Enhanced JSON
 * @module functional-json
 * @typicalname FJSON
 * @license Copyright (c) 2017 Malpaux IoT All Rights Reserved.
 */

/**
 * Serialize the given data structure (including functions)
 * @alias module:functional-json.stringify
 * @param {*} val - The data to be serialized
 * @param {?function} replacer - Optional replacer function
 * @param {?string|number} space - Optional white space insertion
 * @return {string} The serialized result
 */
const stringify = (val, replacer, space) =>
  JSON.stringify(val, (k, v) => {
    let n = typeof replacer === 'function' ? replacer(k, v) : v;
    if (typeof n === 'function') n = n.toString();
    return n;
  }, space);

/**
 * Parse the given JSON string (revive functions)
 * @alias module:functional-json.parse
 * @param {string} str - The string to be parsed
 * @param {?function} reviver - Optional reviver function
 * @return {*} The parsed result
 */
const parse = (str, reviver) =>
  JSON.parse(str, (k, v) => {
    let f = v;
    if (v
      && typeof v === 'string'
      && v.substr(0, 8) === 'function'
    ) {
      try {
        f = new Function( // eslint-disable-line no-new-func
          v.substring(v.indexOf('(') + 1, v.indexOf(')')),
          v.substring(v.indexOf('{') + 1, v.lastIndexOf('}')),
        );
      } catch (ignore) { /* continue */ }
    }

    return typeof reviver === 'function' ? reviver(k, f) : f;
  });

module.exports = {
  stringify,
  parse,
};
