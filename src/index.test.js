import FJSON from './';

it('should serialize an object', () => {
  const o = { key: 'value' };
  expect(FJSON.stringify(o)).toBe(JSON.stringify(o));
});

it('should parse a serialized object', () => {
  const str = JSON.stringify({ key: 'value' });
  expect(FJSON.parse(str)).toEqual(JSON.parse(str));
});

it('should serialize a function', () => {
  const fn = (v) => v + 1;
  expect(FJSON.stringify(fn)).toBe(JSON.stringify(fn.toString()));
});

it('should parse a serialized function', () => {
  const fn = (v) => v + 1;
  expect(FJSON.parse(JSON.stringify(fn.toString()))(0)).toBe(1);
});

it('should serialize and parse an object containing a function', () => {
  const o =
    {
      key: 'value',
      fn: (v) => v + 1,
    },
    res = FJSON.parse(FJSON.stringify(o));
  expect(res.key).toBe(o.key);
  expect(res.fn(0)).toBe(1);
});
