process.env.NODE_ENV = 'production';

var rimrafSync = require('rimraf').sync,
    webpack = require('webpack'),
    config = require('../config/webpack.config.js'),
    paths = require('../config/paths.js');

function logErrors(status, errors) {
  console.log(status);
  console.log();
  errors.forEach(function (err) {
    console.log(err.message || err);
    console.log();
  });
}

function build () {
  rimrafSync(paths.appBuild + '/*');
  webpack(config).run(function (err, stats) {
    if (err) {
      logErrors('Failed to build.', [err]);
      process.exit(1);
    }
    if (stats.compilation.errors.length) {
      logErrors('Failed to build.', stats.compilation.errors);
      process.exit(1);
    }

    console.log('Built successfully.');
  });
}

build();
