# Functional JSON [![wercker status](https://app.wercker.com/status/f3cfa00e02fde92e2b69f4e6f56a7678/s/master "wercker status")](https://app.wercker.com/project/byKey/f3cfa00e02fde92e2b69f4e6f56a7678)

> An enhanced JSON serializer & parser that can encode functions.
>
> **[API Reference](https://bitbucket.org/malpaux/functional-json/wiki/Home)**


## Installing / Getting Started

Install the package
```shell
npm install --save functional-json
```

and import/require it
```javascript
import FJSON from 'functional-json';
// OR (pre ES6)
var FJSON = require('functional-json');
```

### Usage
```javascript
const str = FJSON.stringify({
  key: 'value',
  fn: (v) => v + 1
});

FJSON.parse(str).fn(0); // 1
```

## Developing

This is what you do after you have cloned the repository:

```shell
npm install
npm run build
```

(Install dependencies & build the project.)

### Linting

Execute ESLint

```shell
npm run lint
```

Try to automatically fix linting errors
```shell
npm run lint:fix
```

### Testing

Execute Jest unit tests using

```shell
npm test
```

Tests are defined in the same directory the module lives in. They are specified in '[module].test.js' files.

### Building

To build the project, execute

```shell
npm run build
```

This saves the production ready code into 'dist/'.

### Documentation

The app is documented using JSDoc. To generate docs, use

```shell
npm run docs
```

This saves HTML documentation into 'docs/'. It requires that you have additionally installed ```jsdoc```.


To generate a Markdown API reference, you can alternatively use

```shell
npm run docs-md
```


This saves the documentation into 'docs/index.md'. It requires that you have additionally installed ```jsdoc-to-markdown```.
